#include <EverydayCalendar_lights.h>
#include <EverydayCalendar_touch.h>

typedef struct {
   int8_t    x;
   int8_t    y;
} Point;

EverydayCalendar_touch cal_touch;
EverydayCalendar_lights cal_lights;
int16_t brightness = 128;
bool touchEnabled = true;

void setup() {
  Serial.begin(9600);
  Serial.println(F("Sketch setup started"));

  // Initialize LED functionality
  cal_lights.configure();
  cal_lights.setBrightness(200);
  cal_lights.begin();

  // Perform startup animation
  honeyDrip();

  // Fade out
  for(int b = 200; b >= 0; b--){
    cal_lights.setBrightness(b);
    delay(4);
  }

  // Initialize touch functionality
  cal_touch.configure();
  cal_touch.begin();
  cal_lights.loadLedStatesFromMemory();
  delay(1500);

  // Fade in
  for(int b = 0; b <= brightness; b++){
    cal_lights.setBrightness(b);
    delay(4);
  }

  Serial.println(F("Sketch setup complete"));
}

void loop() {
  // process any serial input
  processSerialInput();

  static Point previouslyHeldButton = {0xFF, 0xFF}; // 0xFF and 0xFF if no button is held
  static uint16_t touchCount = 1;
  static const uint8_t debounceCount = 3;
  static const uint16_t clearCalendarCount = 1300; // ~40 seconds.  This is in units of touch sampling interval ~= 30ms.
  Point buttonPressed = {0xFF, 0xFF};
  bool touch = cal_touch.scanForTouch();

  // Log touch events
  if (previouslyHeldButton.x != -1 && previouslyHeldButton.x != cal_touch.x) {
    Serial.print(F("Release: "));
    Serial.print(previouslyHeldButton.x);
    Serial.print(' ');
    Serial.println(previouslyHeldButton.y);
  }
  if(cal_touch.x != -1 && previouslyHeldButton.x != cal_touch.x) {
    Serial.print(F("Press: "));
    Serial.print(cal_touch.x);
    Serial.print(' ');
    Serial.println(cal_touch.y);
  }

  // Handle a button press
  if(touch)
  {
    // Brightness Buttons
    if(touchEnabled && cal_touch.y == 31){
      if(cal_touch.x == 4){
        brightness -= 3;
      }else if(cal_touch.x == 6){
        brightness += 2;
      }
      brightness = constrain(brightness, 0, 200);
      Serial.print(F("Brightness: "));
      Serial.println(brightness);
      cal_lights.setBrightness((uint8_t)brightness);
    }
    // If all buttons aren't touched, reset debounce touch counter
    if(previouslyHeldButton.x == -1){
      touchCount = 0;
    }

    // If this button is been held, or it's just starting to be pressed and is the only button being touched
    if(((previouslyHeldButton.x == cal_touch.x) && (previouslyHeldButton.y == cal_touch.y))
    || (debounceCount == 0))
    {
      // The button has been held for a certain number of consecutive checks
      // This is called debouncing
      if (touchCount == debounceCount){
        // Button is activated
        if (touchEnabled) {
          cal_lights.toggleLED((uint8_t)cal_touch.x, (uint8_t)cal_touch.y);
          cal_lights.saveLedStatesToMemory();
        }
        Serial.print(F("x: "));
        Serial.print(cal_touch.x);
        Serial.print(F("\ty: "));
        Serial.println(cal_touch.y);
      }

      // Check if the special "Reset" January 1 button is being held
      if(touchEnabled && (cal_touch.x == 0) && (cal_touch.y == 0) && (touchCount == clearCalendarCount)){
        Serial.println(F("Resetting all LED states"));
        clearAnimation();
      }

      if(touchCount < 65535){
        touchCount++;
        Serial.println(touchCount);
      }
    }
  }

  previouslyHeldButton.x = cal_touch.x;
  previouslyHeldButton.y = cal_touch.y;
}

void honeyDrip(){
  uint16_t interval_ms = 25;
  static const uint8_t monthDayOffset[12] = {0, 3, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0};
  // Turn on all LEDs one by one in the span of a few second
  for(int day = 0; day < 31; day++){
    for(int month = 0; month < 12; month++){
      int8_t adjustedDay = day - monthDayOffset[month];
      if(adjustedDay >= 0 ){
        cal_lights.setLED(month, adjustedDay, true);
      }
    }
    delay(interval_ms);
    interval_ms = interval_ms + 2;
  }
}

void clearAnimation(){
  uint16_t interval_ms = 25;
  static const uint8_t monthMaxDay[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
  for(int month = 11; month >= 0; month--){
    for(int day = monthMaxDay[month]-1; day >=0; day--){
       cal_lights.setLED(month, day, false);
       delay(interval_ms);
    }
  }
  cal_lights.saveLedStatesToMemory();
}

enum EchoState { // echo state - determine if received serial characters should be echoed back to the sender
    EchoSuspended = -1, // echo temporarily suspended for current command (when command starts with "!")
    EchoDisabled = 0,
    EchoEnabled = 1
};
enum EchoState echoState = EchoEnabled;

#define SERIAL_BUFFER_SIZE 512
uint8_t serialBuffer[SERIAL_BUFFER_SIZE + 4];
int16_t bufferPos = -1;

void processSerialInput() {
    // asynchronous no-wait serial input handling
    while (Serial.available()) {
      char incomingByte = Serial.read();
      bufferPos++;
      if (bufferPos >= SERIAL_BUFFER_SIZE) {
        if (incomingByte == '\r' || incomingByte == '\n' || incomingByte == '\0') {
          Serial.print(F("Error: Command buffer limit reached"));
          bufferPos = -1;
        }
        continue;
      }
      // add byte to input buffer
      serialBuffer[bufferPos] = incomingByte;
      if (incomingByte == '!' && bufferPos == 0) {
        // if command starts with ! temporarily turn disable echo
        if (echoState == EchoEnabled) {
          echoState = EchoSuspended;
        }
        // reset buffer position to ignore !
        bufferPos = -1;
      }
      if (echoState == EchoEnabled) {
        Serial.print(incomingByte);
        if (incomingByte == '\r') {
          Serial.print('\n');
        }
      }
      if (incomingByte == '\r' || incomingByte == '\n' || incomingByte == '\0') {
        serialBuffer[bufferPos] = '\0'; // mark end of command input
        processSerialCommand();
        bufferPos = -1;
        // reset suspended echo after command
        if (echoState == EchoSuspended) {
          echoState = EchoEnabled;
        }
      }
    }
}

void processSerialCommand() {
  if (strncmp("get ", serialBuffer, 4) == 0
    || strncmp("set ", serialBuffer, 4) == 0
    || strncmp("off ", serialBuffer, 4) == 0
    || strncmp("on ", serialBuffer, 3) == 0) {
    processLightCommand();
    return;
  }

  if (strncmp("echo off", serialBuffer, 8) == 0) {
    Serial.println(F("Echo disabled"));
    echoState = EchoDisabled;
  } else if (strncmp("echo", serialBuffer, 4) == 0) {
    Serial.println(F("Echo enabled"));
    echoState = EchoEnabled;
  }

  if (strncmp("touch off", serialBuffer, 9) == 0) {
    Serial.println(F("Touch disabled"));
    touchEnabled = false;
  } else if (strncmp("touch on", serialBuffer, 8) == 0) {
    Serial.println(F("Touch enabled"));
    touchEnabled = true;
  }

  if (strncmp("load\0", serialBuffer, 5) == 0) {
    cal_lights.loadLedStatesFromMemory();
  }
  if (strncmp("save\0", serialBuffer, 5) == 0) {
    cal_lights.saveLedStatesToMemory();
    Serial.print(F("Saved state"));
  }

  if (strncmp("drip", serialBuffer, 4) == 0) {
    honeyDrip();
    Serial.print(F("Drip completed"));
  }

  if (strncmp("brightness\0", serialBuffer, 11) == 0) {
    Serial.print(F("Brightness: "));
    Serial.println(brightness);
  }
  if (strncmp("brightness ", serialBuffer, 11) == 0) {
    brightness = constrain(atoi(&serialBuffer[11]), 0, 200);
    cal_lights.setBrightness((uint8_t)brightness);
    Serial.print(F("Brightness: "));
    Serial.println(brightness);
  }

  if (strncmp("fade ", serialBuffer, 5) == 0) {
    int b = brightness;
    brightness = constrain(atoi(&serialBuffer[5]), 0, 200);
    if (b <= brightness) {
      for(; b <= brightness; b++){
        cal_lights.setBrightness(b);
        delay(4);
      }
    } else {
      for(; b >= brightness; b--){
        cal_lights.setBrightness(b);
        delay(4);
      }
    }
    Serial.print(F("Brightness: "));
    Serial.println(brightness);
  }

  if (strncmp("baud ", serialBuffer, 5) == 0) {
    int baud = atoi(&serialBuffer[5]);
    if (baud == 9600 || baud == 14400 || baud == 19200 || baud == 28800 || baud == 38400 || baud == 57600 || baud == 115200) {
      Serial.print(F("Baud rate: "));
      Serial.println(baud);
      Serial.end();
      Serial.begin(baud);
    } else {
      Serial.print(F("Invalid baud rate: "));
      Serial.println(baud);
    }
  }
}

void processLightCommand() {
  static const char monthNames[12][4] = {"jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec"};
  static const uint8_t monthMaxDay[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

  boolean isGetCommand = strncmp("get ", serialBuffer, 4) == 0;
  if (isGetCommand) {
    Serial.print(F("Lights: "));
  }

  boolean state = true;
  if (bufferPos > 4 && strncmp(" off", &serialBuffer[bufferPos - 4], 4) == 0) {
    // set state to false if command ends with ' off'
    // allows ex: `set all off` or `set jan 1 off`
    state = false;
  }

  uint16_t cmdPos = 0;
  uint16_t count = 0;
  uint8_t digitCount = 0;
  uint8_t month = 0;
  uint8_t day = 0;
  // loop through entire command
  while (cmdPos <= bufferPos) {
    // set current command state
    if (strncmp("on ", &serialBuffer[cmdPos], 3) == 0) {
        state = true;
    }
    if (strncmp("off ", &serialBuffer[cmdPos], 4) == 0) {
        state = false;
    }

    // check for each month abbreviation
    for (uint8_t monthIndex = 0; monthIndex < 12; monthIndex++) {
      if (strncmp(monthNames[monthIndex], &serialBuffer[cmdPos], 3) == 0) {
        month = monthIndex + 1;
      }
    }

    // get all, or set all leds to state
    if (strncmp("all", &serialBuffer[cmdPos], 3) == 0) {
      for(uint8_t monthIndex = 0; monthIndex < 12 ; monthIndex++){
        if (isGetCommand) {
          if (monthIndex > 0) {
              Serial.print(' ');
          }
          Serial.print(monthNames[monthIndex]);
        }
        for(uint8_t dayIndex = 0; dayIndex < monthMaxDay[monthIndex]; dayIndex++){
           if (isGetCommand) {
             Serial.print(cal_lights.getLED(monthIndex, dayIndex) ? 1 : 0);
           } else {
             cal_lights.setLED(monthIndex, dayIndex, state);
           }
        }
      }
      count += 365;
    }

    // handle multiple digits / days
    if (serialBuffer[cmdPos] >= '0' && serialBuffer[cmdPos] <= '9') {
      digitCount++;
      if (day > 0) {
        day = day * 10;
      }
      day += (uint8_t)(serialBuffer[cmdPos]) - (uint8_t)('0');
    } else {
      if (month > 0 && digitCount >= 3) {
        // if digitCount >= 3, numbers should be a binary string of 01010101... for setting an entire month
        for (day = 1; day <= digitCount; day++) {
          if (serialBuffer[cmdPos - digitCount + day - 1] == '0') {
            cal_lights.setLED(month - 1, day - 1, false);
          }
          if (serialBuffer[cmdPos - digitCount + day - 1] == '1') {
            cal_lights.setLED(month - 1, day - 1, true);
          }
        }
        day = 0;
        count += digitCount;
      }
      digitCount = 0;
    }

    // set LED before next number or end of command
    if (serialBuffer[cmdPos] == ' ' || serialBuffer[cmdPos] == '\0') {
      if (month > 0 && day > 0) {
        if (isGetCommand) {
          Serial.print(cal_lights.getLED(month - 1, day - 1) ? 1 : 0);
        } else {
          cal_lights.setLED(month - 1, day - 1, state);
        }
        count++;
      }
      day = 0;
    }
    cmdPos++;
  }

  if (isGetCommand) {
    Serial.println();
  } else if (echoState == EchoEnabled) {
    Serial.print(count);
    Serial.println(F(" lights set"));
  }
}
